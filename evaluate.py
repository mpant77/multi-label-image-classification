import tensorflow as tf
import os
import time
from multi_label_classification_model import MultiLabelClassificationModel, ModelConfig

# Model config
tf.app.flags.DEFINE_string(
	'mode', 'eval', 'Mode: eval')

tf.app.flags.DEFINE_integer(
	'label_count', None,'Num of labels in data')

tf.app.flags.DEFINE_string(
	'input_tfrecord_file', None, 'Path to input tfrecord file')

tf.app.flags.DEFINE_integer(
	'batch_size', None, 'Batch size of evaluation data')

# Eval config
tf.app.flags.DEFINE_integer(
	'num_eval_examples', None, 'total count of examples in eval dataset')

tf.app.flags.DEFINE_string(
	'checkpoint_dir', None, 'checkpoint dir')

tf.app.flags.DEFINE_integer(
	'min_global_step', 200, 'check min global step')

tf.app.flags.DEFINE_string(
	'eval_dir', None, 'Path to write evaluation summary')

tf.app.flags.DEFINE_integer(
	'eval_interval_secs', 20, 'Delay before evaluating latest ckpt')


FLAGS = tf.app.flags.FLAGS

class EvalConfig(object):
	def __init__(self):
		# total count of examples in eval dataset
		self.num_eval_examples = FLAGS.num_eval_examples

		# checkpoint dir
		self.checkpoint_dir = FLAGS.checkpoint_dir

		# check min global step
		self.min_global_step = FLAGS.min_global_step

		self.batch_size = FLAGS.batch_size

		# eval 
		self.eval_dir = os.path.join(self.checkpoint_dir, FLAGS.eval_dir)
		self.eval_interval_secs = FLAGS.eval_interval_secs
	
	
last_eval_checkpoint_step = None

def evaluate_model(sess, model, global_step, summary_writer, summary_op, eval_config):
	"""computes loss

	Args:
		sess: Session object
		model: Instance of MultiLabelClassificationModel
		global_step: Integer; global step of the model checkpoint
		summary_writer: Instance of FileWriter.
		summary_op: Op for generating model summaries.
		eval_config: Instance of EvalConfig
	"""
	summary_str = sess.run(summary_op)
	summary_writer.add_summary(summary_str, global_step)

	num_eval_batches = int(eval_config.num_eval_examples / eval_config.batch_size)

	sum_losses = 0.0
	start_time = time.time()
	for i in range(num_eval_batches):
		loss = sess.run(model.total_loss)
		tf.logging.info("Computed losses for {} of {} batches: {}".format(i + 1, num_eval_batches, loss))
		sum_losses += loss

	ave_loss = sum_losses / num_eval_batches
	tf.logging.info("Ave loss: {}".format(ave_loss))

	summary = tf.Summary()	
	value = summary.value.add()
	value.simple_value = ave_loss
	value.tag = "AveLoss"
	summary_writer.add_summary(summary, global_step)


def run_once(model, saver, summary_writer, summary_op, eval_config):
	"""Evaluates the latest model checkpoint.

	Args:
		model: Instance of MultiLabelClassificationModel to evaluate
		saver: Instance of tf.train.Saver for restoring the model Variables
		summary_writer: Instance of FileWriter.
		summary_op: Op for generating model summaries.
		eval_config: Instance of EvalConfig.
	"""
	global last_eval_checkpoint_step
	model_path = tf.train.latest_checkpoint(eval_config.checkpoint_dir)
	if not model_path:
		tf.logging.info("Skipping evaluation. No checkpoint found in: %s", eval_config.checkpoint_dir)
		return

	# gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.30)
	# config = tf.ConfigProto(gpu_options=gpu_options)
	with tf.Session() as sess:
		# Load model from checkpoint
		tf.logging.info("Loading model from checkpoint: %s", model_path)
		saver.restore(sess, model_path)
		global_step = tf.train.global_step(sess, model.global_step.name)
		tf.logging.info("Successfully loaded %s at global step = %d.", os.path.basename(model_path), global_step)

		if last_eval_checkpoint_step == global_step:
			tf.logging.info("No new checkpoint created")
			return
		
		last_eval_checkpoint_step = global_step

		# check min global step
		if global_step < eval_config.min_global_step:
			tf.logging.info("Skipping evaluation. Global step = %d < %d", global_step, eval_config.min_global_step)
			return

		# Start the queue runners
		coord = tf.train.Coordinator()
		threads = tf.train.start_queue_runners(coord=coord)

		# Run evaluation on the latest checkpoint
		try:
			evaluate_model(
				sess=sess,
				model=model,
				global_step=global_step,
				summary_writer=summary_writer,
				summary_op=summary_op,
				eval_config=eval_config)
		except Exception as e:
			tf.logging.error("Evaluation failed.")
			coord.request_stop(e)

		coord.request_stop()
		coord.join(threads, stop_grace_period_secs=10)
		


def main(_):
	# -- start : modify the model_config and train_config --
	# model config
	model_config = ModelConfig(
		mode=FLAGS.mode,
		label_count=FLAGS.label_count,
		input_tfrecord_file=FLAGS.input_tfrecord_file,
		batch_size=FLAGS.batch_size
	)

	# eval config
	eval_config = EvalConfig()
	eval_config.batch_size = model_config.batch_size
	# -- end: modify the model_config and train_config --


	# # no need to use GPU
	os.environ['CUDA_VISIBLE_DEVICES'] = ""

	# set log level
	tf.logging.set_verbosity(tf.logging.INFO)

	# prepare eval dir
	if not tf.gfile.IsDirectory(eval_config.eval_dir):
		tf.logging.info("Creating eval directory: %s", eval_config.eval_dir)
		tf.gfile.MakeDirs(eval_config.eval_dir)


	g = tf.Graph()
	with g.as_default():
		# build model
		model = MultiLabelClassificationModel(model_config)
		model.build()

		# Create saver to restore model variables
		saver = tf.train.Saver()

		# Create the summary operation and the summary writer.
		summary_op = tf.summary.merge_all()
		summary_writer = tf.summary.FileWriter(eval_config.eval_dir, graph=g)

		g.finalize()

		while True:
			start = time.time()
			tf.logging.info("Starting evaluation at " + time.strftime("%Y-%m-%d-%H:%M:%S", time.localtime()))
			run_once(model, saver, summary_writer, summary_op, eval_config)
			time_to_next_eval = start + eval_config.eval_interval_secs - time.time()
			if time_to_next_eval > 0:
				time.sleep(time_to_next_eval)


if __name__ == '__main__':
	tf.app.run()
	# main()



'''
images = sess.run("batch:0")
labels = sess.run("batch:1")
g_t = sess.run("Cast:0")


resnet_in = sess.run("resnet_v2_50/Pad:0")
resnet_out = sess.run("resnet_v2_50/postnorm/Relu:0")
conv1_out = sess.run("my_sub_network/my_conv_1/Relu:0")
conv2_out = sess.run("my_sub_network/my_conv_2/Relu:0")
conv3_out = sess.run("my_sub_network/my_conv_3/BiasAdd:0")
logits_out = sess.run("my_sub_network/logits:0")

import numpy as np
image_layer_out = np.array([resnet_in, resnet_out, conv1_out, conv2_out, conv3_out, logits_out])

Cast:0, batch:1
encoded_image = sess.run("ParseSingleSequenceExample/ParseSingleSequenceExample:0")
decoded_image = sess.run("decode_image/convert_image:0")
resnet_in = sess.run("resnet_v2_50/Pad:0")



'decode_image/Mul'
'''
