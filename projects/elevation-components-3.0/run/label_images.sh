#!/usr/bin/env bash

python ../../label_images.py \
    --label_count 9 \
    --label_file data/labels.txt \
    --imglist_file=data/test.txt \
    --checkpoint_path=train_dir/ec-3.0.1/model.ckpt-38633 \
    --inference_dir=infer_dir \
    --inference_file_name=pred-ec-3.0.1-step38633.txt