#!/usr/bin/env bash

python ../../train.py \
    --label_count 9 \
    --base_network_checkpoint=../../models/resnet_v2_50_2017_04_14/resnet_v2_50.ckpt \
    --input_tfrecord_file=data/train/train.tfrecords \
    --train_dir=train_dir/ec-3.0.1 \
    --learning_rate 0.01 \
    --batch_size 16 \
    --train_base_network=False \
    --optimizer=Adam\
    --number_of_steps 80000 \
    --save_summaries_secs 60 \
    --save_interval_secs 180