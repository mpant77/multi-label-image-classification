#!/usr/bin/env bash

python ../../inference.py \
    --label_count 9 \
    --imglist_file=data/valid/image_list.txt \
    --checkpoint_path=train_dir/ec-3.0.1/model.ckpt-43662 \
    --inference_dir=infer_dir \
    --inference_file_name=inf-3.0.1-step43662.txt \
