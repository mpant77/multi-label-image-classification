#!/usr/bin/env bash

python ../../evaluate.py \
    --label_count 9 \
    --input_tfrecord_file=data/valid/valid.tfrecords \
    --num_eval_examples 809 \
    --checkpoint_dir=train_dir/ec-3.0.1 \
    --batch_size 30 \
    --eval_dir=eval