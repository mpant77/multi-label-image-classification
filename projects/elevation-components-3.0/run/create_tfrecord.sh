#!/usr/bin/env bash

#train
python ../../create_tfrecord.py \
    --image_dir=""\
    --imglist_file="data/train/image_list.txt" \
    --imglabel_file="data/train/image_label.txt" \
    --output_file="data/train/train.tfrecords" \
    --gpu="0"

##valid
#python ../../create_tfrecord.py \
#    --image_dir=""\
#    --imglist_file="data/valid/image_list.txt" \
#    --imglabel_file="data/valid/image_label.txt" \
#    --output_file="data/valid/valid.tfrecords" \
#    --gpu="0"