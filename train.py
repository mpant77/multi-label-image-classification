import tensorflow as tf
from multi_label_classification_model import MultiLabelClassificationModel, ModelConfig
import os

# Model config
tf.app.flags.DEFINE_string(
	'mode', 'train', 'Mode: train')

tf.app.flags.DEFINE_integer(
	'label_count', None,'Num of labels in data')

tf.app.flags.DEFINE_string(
	'base_network_checkpoint', '', 'Path to base ckpt file')

tf.app.flags.DEFINE_string(
	'input_tfrecord_file', '', 'Path to input tfrecord file')

tf.app.flags.DEFINE_integer(
	'batch_size', None,'Batch size for training')

tf.app.flags.DEFINE_bool(
	'train_base_network', False ,'train_base_network')

# Train config
tf.app.flags.DEFINE_string(
	'train_dir', None, 'Path to training dir folder')

tf.app.flags.DEFINE_float(
	'learning_rate', 0.1, 'learning_rate')

# params for learning rate exponential_decay
tf.app.flags.DEFINE_integer(
	'decay_steps', 2000, 'Number of steps after which l-r decays')

tf.app.flags.DEFINE_float(
	'decay_rate', 0.9, 'Rate of decay of l-r')

tf.app.flags.DEFINE_bool(
	'staircase', True, 'Staircase type of decay')

# optimizer
tf.app.flags.DEFINE_string(
	'optimizer', 'adam', 'optimizer: Adam, Momentum, RMSProp, Adagrad, Ftrl, SGD')

tf.app.flags.DEFINE_float(
	'clip_gradients', 5.0, 'clip gradients')

# saver
tf.app.flags.DEFINE_integer(
	'max_to_keep', 50, 'Max num of ckpt to keep')

tf.app.flags.DEFINE_float(
	'keep_checkpoint_every_n_hours', 0.2, 'Keep atleast one checkpoint from every n hours')

#train steps
tf.app.flags.DEFINE_integer(
	'number_of_steps', 10000, 'Number of steps to run training')

tf.app.flags.DEFINE_integer(
	'save_summaries_secs', 120, 'time delay for saving summary')

tf.app.flags.DEFINE_integer(
	'save_interval_secs', 360, 'time delay for saving checkpoint')

FLAGS = tf.app.flags.FLAGS

class TrainConfig(object):
	def __init__(self):
		# dir to save checkpoint
		self.train_dir = FLAGS.train_dir

		# GPU ID
		self.gpu_id = "0"
		
		# learning rate
		self.learning_rate = FLAGS.learning_rate

		# params for learning rate exponential_decay
		self.decay_steps = FLAGS.decay_steps
		self.decay_rate = FLAGS.decay_rate
		self.staircase = FLAGS.staircase

		# optimizer
		self.optimizer = FLAGS.optimizer
		self.clip_gradients = FLAGS.clip_gradients

		# saver
		self.max_to_keep = FLAGS.max_to_keep
		self.keep_checkpoint_every_n_hours = FLAGS.keep_checkpoint_every_n_hours

		# train steps
		self.number_of_steps = FLAGS.number_of_steps
		

def main(_):
	# -- start : modify the model_config and train_config --
	# model config
	model_config = ModelConfig(
		mode=FLAGS.mode,
		label_count=FLAGS.label_count,
		base_network_checkpoint=FLAGS.base_network_checkpoint,
		input_tfrecord_file=FLAGS.input_tfrecord_file,
		batch_size=FLAGS.batch_size,
		train_base_network=FLAGS.train_base_network,
	)

	# train config
	train_config = TrainConfig()

	# -- end: modify the model_config and train_config --

	
	# set GPU
	os.environ['CUDA_VISIBLE_DEVICES'] = train_config.gpu_id 

	# set log level
	tf.logging.set_verbosity(tf.logging.INFO)

	# prepare train dir
	if not tf.gfile.IsDirectory(train_config.train_dir):
		tf.logging.info("Creating training directory: %s", train_config.train_dir)
		tf.gfile.MakeDirs(train_config.train_dir)

	g = tf.Graph()
	with g.as_default():

		# build model
		model = MultiLabelClassificationModel(model_config)
		model.build()

		def _learning_rate_decay_fn(learning_rate, global_step):
			return tf.train.exponential_decay(
				learning_rate,
				global_step,
				decay_steps=train_config.decay_steps,
				decay_rate=train_config.decay_rate,
				staircase=train_config.staircase)
		
		learning_rate_decay_fn = _learning_rate_decay_fn

		# Set up the training ops
		train_op = tf.contrib.layers.optimize_loss(
			loss=model.total_loss,
			global_step=model.global_step,
			learning_rate=train_config.learning_rate,
			optimizer=train_config.optimizer,
			clip_gradients=train_config.clip_gradients,
			learning_rate_decay_fn=learning_rate_decay_fn)

		# Set up the saver for saving and restoring model checkpoints.
		saver = tf.train.Saver(
			max_to_keep=train_config.max_to_keep,
			keep_checkpoint_every_n_hours=train_config.keep_checkpoint_every_n_hours)

		gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
		config = tf.ConfigProto(gpu_options=gpu_options)

		# Run training.
		tf.contrib.slim.learning.train(
			train_op,
			train_config.train_dir,
			log_every_n_steps=10,
			graph=g,
			global_step=model.global_step,
			number_of_steps=train_config.number_of_steps,
			init_fn=model.base_network_init_fn,
			saver=saver,
			session_config=config,
			save_summaries_secs=FLAGS.save_summaries_secs,
			save_interval_secs=FLAGS.save_interval_secs,
		)


if __name__ == "__main__":
	tf.app.run()
	# parser = argparse.ArgumentParser(description='Train parameters')
	# # Model config
	# parser.add_argument('--mode', required=True, help='Mode: train', type=str)
	# parser.add_argument('--label_count', required=True, help='Num of labels in data', type=int)
	# parser.add_argument('--base_network_checkpoint', required=True, help='Path to base ckpt file', type=str)
	# parser.add_argument('--input_tfrecord_file', required=True, help='Path to input tfrecord file', type=str)
	# # Train config
	# parser.add_argument('--train_dir', required=True, help='Path to training dir folder', type=str)
	# parser.add_argument('--learning_rate', required=True, help='learning_rate', type=float)
	# # params for learning rate exponential_decay
	# parser.add_argument('--decay_steps', required=False, help='Number of steps after which l-r decays', type=int, default=500)
	# parser.add_argument('--decay_rate', required=False, help='Rate of decay of l-r', type=float, default=0.9)
	# parser.add_argument('--staircase', required=False, help='Staircase type of decay', type=bool, default=True)
	#
	# parser.add_argument('--optimizer', required=True, help='optimizer: adam', type=str)
	# parser.add_argument('--clip_gradients', required=True, help='clip gradients', type=float)
	# parser.add_argument('--max_to_keep', required=True, help='Max num of ckpt to keep', type=int)
	# parser.add_argument('--keep_checkpoint_every_n_hours', required=True, help='Keep atleast one checkpoint from every n hours', type=float)
	# parser.add_argument('--save_summaries_secs', required=True, help='time delay for saving summary', type=int)
	# parser.add_argument('--save_interval_secs', required=True, help='time delay for saving checkpoint', type=int)
	#
	#
	# args = parser.parse_args()
	# main()
