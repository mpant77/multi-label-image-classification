#!/usr/bin/env bash
python threshold_calibration.py \
    --label_file="projects/elevation-components-3.0/data/valid/image_label.txt" \
    --score_file="projects/elevation-components-3.0/infer_dir/inf-3.0.1-step43662.txt"