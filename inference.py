import tensorflow as tf
from multi_label_classification_model import MultiLabelClassificationModel, ModelConfig
import os
import numpy as np

# Model config
tf.app.flags.DEFINE_string(
    'mode', 'inference', 'Mode: inference')

tf.app.flags.DEFINE_integer(
    'label_count', None, 'Num of labels in data')

tf.app.flags.DEFINE_string(
    'imglist_file', None, 'Path to file contating image list')

# Inference config
tf.app.flags.DEFINE_string(
    'checkpoint_path', None, 'Path to the ckpt file to be used for inference')

tf.app.flags.DEFINE_string(
    'inference_dir', None, 'Path in which dir inference result file will be saved in')

tf.app.flags.DEFINE_string(
    'inference_file_name', None, 'File in which inference result will be saved in')


FLAGS = tf.app.flags.FLAGS

class InferenceConfig(object):
    def __init__(self):
        # checkpoint path.
        self.checkpoint_path = FLAGS.checkpoint_path

        # in which dir inference result file will be saved in.
        self.inference_dir = FLAGS.inference_dir

        # in which file inference result will be saved in
        self.inference_file_name = FLAGS.inference_file_name

        # gpu id
        self.gpu_id = "0"

def get_test_image_list(imglist_file):
    imglist = np.loadtxt(imglist_file, dtype=str)
    return list(imglist[:,0])


def main(_):
    # -- start : modify the model_config and train_config --
    # model config
    model_config = ModelConfig(
        mode=FLAGS.mode,
        label_count=FLAGS.label_count)
    model_config.resize_height = 290
    model_config.resize_width = 290

    imglist_file = FLAGS.imglist_file

    # inference config
    inference_config = InferenceConfig()
    # -- end: modify the model_config and train_config --

    # set log level
    tf.logging.set_verbosity(tf.logging.INFO)

    # inference images list
    images = get_test_image_list(imglist_file)

    # no need to use GPU
    #os.environ['CUDA_VISIBLE_DEVICES'] = ""
    os.environ['CUDA_VISIBLE_DEVICES'] = inference_config.gpu_id

    g = tf.Graph()
    with g.as_default():
        # build model
        model = MultiLabelClassificationModel(model_config)
        model.build()

        # find checkpoint file
        if tf.gfile.IsDirectory(inference_config.checkpoint_path):
            checkpoint_file = tf.train.latest_checkpoint(inference_config.checkpoint_path)
            if not checkpoint_file:
                raise ValueError("No checkpoint file found in {}".format(inference_config.checkpoint_path))

        else:
            checkpoint_file = inference_config.checkpoint_path

        # make sure inference_dir is ready
        if not tf.gfile.IsDirectory(inference_config.inference_dir):
            tf.logging.info("Creating result directory: %s", inference_config.inference_dir)
            tf.gfile.MakeDirs(inference_config.inference_dir)

        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.50)
        config = tf.ConfigProto(gpu_options=gpu_options)
        with tf.Session(graph=g, config=config) as sess:

            # Set up the saver for restoring model checkpoint.
            saver = tf.train.Saver()
            tf.logging.info("Loading model from checkpoint: %s", checkpoint_file)
            saver.restore(sess, checkpoint_file)
            tf.logging.info("Successfully loaded checkpoint: %s", checkpoint_file)
            g.finalize()

            result = []
            for image_path in images:
                tf.logging.info("inference file: {}".format(image_path))

                # read image
                with tf.gfile.GFile(image_path, 'rb') as f:
                    encoded_image = f.read()

                # run model
                sigmoid_result = sess.run(model.sigmoid_result, feed_dict={model.image_feed: encoded_image})

                after_mean = np.mean(sigmoid_result, axis=0)
                # logits = sess.run(model.logits, feed_dict={model.image_feed: encoded_image})
                #
                # after_mean = np.mean(logits, axis=0)

                print(after_mean)
                result.append(after_mean)

    # save the result
    file_path = os.path.join(inference_config.inference_dir, inference_config.inference_file_name)

    tf.logging.info("save result to: %s", file_path)

    #print(result)
    np.savetxt(file_path, result, fmt='%.6f')

if __name__ == '__main__':
    tf.app.run()
# main()
