import numpy as np


inf_file_path = 'projects/elevation-components-2.0/infer_dir/inf-2.0.4-step19207.txt'
label_file_path ='projects/elevation-components-2.0/data/valid/image_label.txt'

OUTPUT_METRICS_PATH = 'projects/elevation-components-2.0/infer_dir/metrics-2.0.4-step19207-ot.txt'
ALL_LABELS_FILE = 'projects/elevation-components-2.0/data/labels.txt'

num_classes = 4

result = np.loadtxt(fname=inf_file_path, dtype=float)
ground_truth = np.loadtxt(fname=label_file_path, dtype=int)

assert result.shape==ground_truth.shape, "Incorrect inference or label file"

# Apply optimum threshold to inference score
#optimal_threshold = [0.5] * num_classes
optimal_threshold = [0.41, 0.61, 0.77, 0.22]
threshold = np.array([optimal_threshold] * result.shape[0])
prediction = result - threshold
prediction[prediction > 0] = 1
prediction[prediction < 0] = 0

# Calculate performance metrics
tp_ = prediction * ground_truth
fp_ = prediction * (1 - ground_truth)
fn_ = (1 - prediction) * ground_truth
tn_ = (1 - prediction) * (1 - ground_truth)

correct_prediction = np.equal(prediction, ground_truth)

accuracy = np.mean(correct_prediction.astype(float))

all_labels_correct = np.amin(correct_prediction.astype(float), 1)
exact_match_ratio = np.mean(all_labels_correct)

precision = np.count_nonzero(tp_) / ( np.count_nonzero(tp_) + np.count_nonzero(fp_) )

recall = np.count_nonzero(tp_) / (np.count_nonzero(tp_) + np.count_nonzero(fn_))

f1 =  2 * precision * recall / (precision + recall)

precision_per_class = np.count_nonzero(tp_, axis=0) / (np.count_nonzero(tp_, axis=0) + np.count_nonzero(fp_, axis=0))
recall_per_class = np.count_nonzero(tp_, axis=0) / (np.count_nonzero(tp_, axis=0) + np.count_nonzero(fn_, axis=0))
f1_per_class = 2 * precision_per_class * recall_per_class / (precision_per_class + recall_per_class)

#Fetch class labels
with open(ALL_LABELS_FILE, 'r') as f:
    labels = f.read().splitlines()

#Write metrics to the output file
with open(OUTPUT_METRICS_PATH, 'w+') as f:
    f.write('accuracy = %.1f%%\n' % (accuracy * 100))
    f.write('exact match ratio = %.1f%%\n' % (exact_match_ratio * 100))
    f.write('precision = %.1f%%\n' % (precision * 100))
    f.write('recall = %.1f%%\n' % (recall * 100))
    f.write('f1 = %.1f%%\n' % (f1 * 100))
    f.write('\nTest metrics per class:\n')
    for label, prec, rec, f1 in zip(labels, precision_per_class, recall_per_class, f1_per_class):
        f.write('%s (prec = %.1f%%, rec = %.1f%%, f1 = %.1f%%,)\n' % (label, prec * 100, rec * 100, f1 * 100))

print('accuracy = %.1f%%' % (accuracy * 100))
print('exact match ratio = %.1f%%' % (exact_match_ratio * 100))
print('precision = %.1f%%' % (precision * 100))
print('recall = %.1f%%' % (recall * 100))
print('f1 = %.1f%%' % (f1 * 100))
print('\nTest metrics per class:\n')
for label, prec, rec, f1 in zip(labels, precision_per_class, recall_per_class, f1_per_class):
  print('%s (prec = %.1f%%, rec = %.1f%%, f1 = %.1f%%,)\n' % (label, prec * 100, rec * 100, f1 * 100))